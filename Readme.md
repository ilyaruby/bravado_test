# Дока
Created Friday 12 October 2018

##### Разворачивание

Для начала склонируем репозиторий к себе (360 мб)

```git clone [git@bitbucket.org:ilyaruby/bravado_test.git](mailto:git@bitbucket.org:ilyaruby/bravado_test.git)```

Построим образы

```docker-compuse up --build -d```

Проверим статус

```
$ docker-compose ps
   Name                          Command               State           Ports         

*****

bravado_test_autocomplete_1   bundle exec rails s -p 300 ...   Up      0.0.0.0:3001->3001/tcp
bravado_test_db_1             docker-entrypoint.sh postgres    Up      0.0.0.0:5432->5432/tcp
bravado_test_web_1            bundle exec rails s -p 300 ...   Up      0.0.0.0:3000->3000/tcp
```

Все, формально работает и дополнение и CRUD, однако база пока пуста и ее нужно заполнить.

##### Первоначальная загрузка базы

В продакшене я бы так не делал, но я добавил дамп базы и Rake-задачу для восстановления прямо в контейнеры.

Вообще, это не обязательный шаг, т.к. можно "просто" сделать rake snapshot:update, однако на медленных машинах это может занять больше часа.

Выполняем (40 секунд):

```docker exec -ti bravado_test_autocomplete_1 rake db:restore```

Выведется 11 ошибок создания (это удаление таблиц и баз, которых нет), это ОК.

Проверим сколько записей подгрузилось

```docker exec -ti bravado_test_autocomplete_1 rails runner 'puts Company.count'```

Должно быть около 4 млн.

##### Проверка автодополнения

```
$ time curl '<http://127.0.0.1:3001/autocomplete?term=goog>'

{"status":"ok","result":[{"title":"BABYGOOG LTD","url":"","logo":""},{"title":"BABYGOOGO LTD","url":"","logo":""},{"title":"BI-GOOGLE LIMITED","url":"","logo":""},{"title":"BRIAN MCGOOGAN LTD","url":"","logo":""},{"title":"C G GOOGOLPLEX LIMITED","url":"","logo":""},{"title":"GOOGOLPLEX VENTURES LP","url":"","logo":""},{"title":"GOOGA BABYWEAR LTD","url":"","logo":""},{"title":"GOOGABAY LIMITED","url":"","logo":""},{"title":"GOOGIE'S FLOWERS LIMITED","url":"","logo":""},{"title":"GOOGIES LIMITED","url":"","logo":""}]}curl '<http://127.0.0.1:3001/autocomplete?term=goog>'

0.00s user 0.00s system 2% cpu 0.215 total
```
Смотрим на время выполнения, на довольно-таки слабой машине это 0.015 сек, из них база порядка 0.008 сек.

##### CRUD

Стандартный рельсовый CRUD, потратил примерно 30 минут здесь, добавив лишь пагинацию и отладив формы.

Откроем его

```chrome <http://127.0.0.1:3000>```

##### Автозагрузка обновлений

Вот на эту часть я потратил большую часть времени.

Я оформил загрузку в виде rake-задачи и нескольких сервисов, скачивающих снапшот и проверяющих его на новизну.

При повторном запуске проверится md5-сумма образа и попыток вставки не будет.

Конечно же, при рестарте контейнера эта оптимизация будет работать со второго раза.

Итак, выполним загрузку:

```
$ docker exec -ti bravado_test_autocomplete_1 rake [snapshot:update]()

Check if directory exists: ./data
Checking md5sum utility...
Checking unzip utility...
Parsing the website to get a snapshot file name...
...
Downloaded 25 MB so far...
Downloaded 26 MB so far...
...
Downloaded!
Calculating previous snapshot md5 checksum...
md5sum: ./data/latest_snapshot.zip: No such file or directory
Calculating new snapshot md5 checksum...
Old snapshot checksum: 
New snapshot checksum: 3b4d12eeb65dcb59b6a7906f61e6c709
Extracting BasicCompanyDataAsOneFile-2018-10-01.csv from ./data/latest_snapshot.tmp...
Archive:  ./data/latest_snapshot.tmp
  inflating: ./data/BasicCompanyDataAsOneFile-2018-10-01.csv  
Done
Checking extracted snapshot
Extracted to: ./data/BasicCompanyDataAsOneFile-2018-10-01.csv
Snapshot filename: ./data/BasicCompanyDataAsOneFile-2018-10-01.csv
[2018-10-12 13:43:26 +0000] Reading record 10000
[2018-10-12 13:43:35 +0000] Reading record 20000
[2018-10-12 13:43:44 +0000] Reading record 30000
```

На Core i7 и уж тем более на Xeon задача при первом запуске будет укладываться в час.

Есть и более изощренные оптимизации (например, параллельная загрука), которую я решил пока не делать.

##### Комментарии

Я выбрал GIN индекс, т.к. он значительно быстрее GIST для lookup, но медленнее для изменения.
Я использовал триграммы, т.к. full_text search был явным перебором для этой задачи, к тому же с нужными индексами скорость оказалась достаточной.
Также оверкиллом показался ElasticSearch, хотя его и использует для поиска каждая вторая компания, в т.ч. для автодополнения.
Я использовал gem activerecord_import и уникальный ключ по company_number с игнорированием ошибок вставки как самый быстрый известный мне метод.
Можно было ускорить еще больше, деля CSV на порции и обрабатывая или через concurrent_ruby (fork) или через Sidekiq.

##### Тесты

В этот раз я решил обойтись без TDD, т.к. задачи были больше девопсовские и R&D, а не обычные.
Тестировать CRUD смысла нет.
Для автодополнения же можно сделать интеграционный спек, однако я решил пока не заниматься этим и показать что есть.

##### UI

Я решил пока не делать UI, хотя напрашивается поиск в сам CRUD.

##### Решения, которые не подошли


* Сервер Roda для autocomplete как более быстрый, чем Rails и даже Sinatra.


Я потратил полчаса, разворачивая roda-dry-rom-stack, но потом понял, что нужды еще больше ускорять веб для автодополнения нет.
К тому же, гораздо проще строить два контейнера из одного репозитория с одним набором моделей.


* поэлементная вставка на Ruby через FIND_OR_CREATE: наименее эффективный, хотя и гибкий метод. Однако rake-задачу я оставил в репозитории.


##### Неопробованные решения

Можно было попробовать COPY в постгресе, возможно удалось бы заставить его игнорировать записи, не проходящие через уникальный индекс.

Теоретически, это бы еще в несколько раз ускорило работы импортера.

##### Скидки

Некоторые вещи я не делал так, как сделал бы в продакшене.

Например:


* в db/ имеется дамп базы, который закидывается каждый в контейнер (web + autocomplete)
* вместо логгера я использовал puts, завернутый в метод log
* все базы и контейнеры запущены с RAILS_ENV development, обратил внимание позже, решил не менять


##### Вызовы

Труднее всего было отладить импорт CSV на скорость и автоматизм импорта.
Изначально этот пункт задания казался менее важным, т.к. снапшоты на сайте обновляются раз в месяц.
При поэлементной вставке у меня уходило 3.5 часа.
Однако потом я заметил, что в требованиях стоит "раз в час", оптимизировал до 50 минут (на слабой машине).

