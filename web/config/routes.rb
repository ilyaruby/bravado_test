Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  get 'autocomplete' => 'autocomplete#search'
  resources 'companies'

  root :to => 'companies#index'
end
