# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2018_10_12_105954) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "pg_trgm"
  enable_extension "plpgsql"

  create_table "companies", force: :cascade do |t|
    t.string "company_name"
    t.string "company_number", null: false
    t.string "reg_address_care_of"
    t.string "reg_address_po_box"
    t.string "reg_address_address_line1"
    t.string "reg_address_address_line2"
    t.string "reg_address_post_town"
    t.string "reg_address_county"
    t.string "reg_address_country"
    t.string "reg_address_post_code"
    t.string "company_category"
    t.string "company_status"
    t.string "country_of_origin"
    t.string "dissolution_date"
    t.string "incorporation_date"
    t.string "accounts_account_ref_day"
    t.string "accounts_account_ref_month"
    t.string "accounts_next_due_date"
    t.string "accounts_last_made_up_date"
    t.string "accounts_account_category"
    t.string "returns_next_due_date"
    t.string "returns_last_made_up_date"
    t.string "mortgages_num_mort_charge"
    t.string "mortgages_num_mort_outstanding"
    t.string "mortgages_num_mort_part_satisfied"
    t.string "mortgages_num_mort_satisfied"
    t.string "sic_code_sic_text_1"
    t.string "sic_code_sic_text_2"
    t.string "sic_code_sic_text_3"
    t.string "sic_code_sic_text_4"
    t.string "limited_partnerships_num_gen_partner"
    t.string "limited_partnerships_num_lim_partner"
    t.string "uri"
    t.string "previous_name_1_condate"
    t.string "previous_name_1_company_name"
    t.string "previous_name_2_condate"
    t.string "previous_name_2_company_name"
    t.string "previous_name_3_condate"
    t.string "previous_name_3_company_name"
    t.string "previous_name_4_condate"
    t.string "previous_name_4_company_name"
    t.string "previous_name_5_condate"
    t.string "previous_name_5_company_name"
    t.string "previous_name_6_condate"
    t.string "previous_name_6_company_name"
    t.string "previous_name_7_condate"
    t.string "previous_name_7_company_name"
    t.string "previous_name_8_condate"
    t.string "previous_name_8_company_name"
    t.string "previous_name_9_condate"
    t.string "previous_name_9_company_name"
    t.string "previous_name_10_condate"
    t.string "previous_name_10_company_name"
    t.string "conf_stmt_next_due_date"
    t.string "conf_stmt_last_made_up_date"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["company_name"], name: "companies_on_company_name_idx", opclass: :gin_trgm_ops, using: :gin
    t.index ["company_number"], name: "index_companies_on_company_number", unique: true
  end

end
