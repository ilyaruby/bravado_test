class AddIndexToCompanies < ActiveRecord::Migration[5.2]
  def change
    execute 'CREATE INDEX companies_on_company_name_idx ON companies USING GIN(company_name gin_trgm_ops);'
  end
end
