class AddUniqueIndexToCompanies < ActiveRecord::Migration[5.2]
  def change
    add_index :companies, :company_number, unique: true
  end
end
