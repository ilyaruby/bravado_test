class CreateCompanies < ActiveRecord::Migration[5.2]
  def change
    create_table :companies do |t|
      t.string :company_name
      t.string :company_number, unique: true, null: false
      t.string :reg_address_care_of
      t.string :reg_address_po_box
      t.string :reg_address_address_line1
      t.string :reg_address_address_line2
      t.string :reg_address_post_town
      t.string :reg_address_county
      t.string :reg_address_country
      t.string :reg_address_post_code
      t.string :company_category
      t.string :company_status
      t.string :country_of_origin
      t.string :dissolution_date
      t.string :incorporation_date
      t.string :accounts_account_ref_day
      t.string :accounts_account_ref_month
      t.string :accounts_next_due_date
      t.string :accounts_last_made_up_date
      t.string :accounts_account_category
      t.string :returns_next_due_date
      t.string :returns_last_made_up_date
      t.string :mortgages_num_mort_charge
      t.string :mortgages_num_mort_outstanding
      t.string :mortgages_num_mort_part_satisfied
      t.string :mortgages_num_mort_satisfied
      t.string :sic_code_sic_text_1
      t.string :sic_code_sic_text_2
      t.string :sic_code_sic_text_3
      t.string :sic_code_sic_text_4
      t.string :limited_partnerships_num_gen_partner
      t.string :limited_partnerships_num_lim_partner
      t.string :uri
      t.string :previous_name_1_condate
      t.string :previous_name_1_company_name
      t.string :previous_name_2_condate
      t.string :previous_name_2_company_name
      t.string :previous_name_3_condate
      t.string :previous_name_3_company_name
      t.string :previous_name_4_condate
      t.string :previous_name_4_company_name
      t.string :previous_name_5_condate
      t.string :previous_name_5_company_name
      t.string :previous_name_6_condate
      t.string :previous_name_6_company_name
      t.string :previous_name_7_condate
      t.string :previous_name_7_company_name
      t.string :previous_name_8_condate
      t.string :previous_name_8_company_name
      t.string :previous_name_9_condate
      t.string :previous_name_9_company_name
      t.string :previous_name_10_condate
      t.string :previous_name_10_company_name
      t.string :conf_stmt_next_due_date
      t.string :conf_stmt_last_made_up_date
      t.timestamps
    end
  end
end
