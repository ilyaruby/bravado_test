class AutocompleteController < ApplicationController
  def search
    j = ActiveModel::SerializableResource.new(
      Company.where('company_name ILIKE ?', "%#{query}%").limit(10),
      each_serializer: CompaniesSuggestionsSerializer
    )
    render json: {status: 'ok', result: j}
  end

  def query
    permitted_params[:term]
  end

  def permitted_params
    params.permit(:term)
  end
end
