class CompaniesSuggestionsSerializer < ActiveModel::Serializer
	attributes :title, :url, :logo

	def title
    object.company_name
	end

	def url
		''
	end

	def logo
		''
	end
end
