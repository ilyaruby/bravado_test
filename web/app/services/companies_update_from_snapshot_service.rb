class CompaniesUpdateFromSnapshotService
  def work
    downloader = CompaniesSnapshotDownloaderService.new
    extracted_csv_filename = downloader.work
    log "Extracted to: #{extracted_csv_filename}"
    CompaniesSnapshotImporterService.new(extracted_csv_filename).work
    downloader.finalize
    log 'Done'
  rescue => e
    log "Error occured: #{e}"
  end

  private

  def log(text)
    puts text
  end
end
