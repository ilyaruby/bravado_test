require 'open-uri'

class CompaniesSnapshotDownloaderService
  BASE_URL='http://download.companieshouse.gov.uk/'
  OUTPUT_DIR='./data'
  OUTPUT_FILENAME_TMP='latest_snapshot.tmp'
  OUTPUT_FILENAME_ZIP='latest_snapshot.zip'
  MB = 1024 * 1024
  ARCHIVED_FILENAME_PATTERN='BasicCompanyDataAsOneFile.*csv'

  def work
    check_output_dir
    check_md5_util
    check_zip_util

    log "Parsing the website to get a snapshot file name..."
    name = get_file_name

    download_snapshot_with_progress(name)
    if check_snapshots_md5
      puts 'MD5 checksums matches, no need to update. Exiting'
      return
    end

    extract_csv_from_zip(name)
  end

  def finalize
    log "Updating snapshot version on disk"
    system("mv #{snapshot_full_path_tmp} #{snapshot_full_path}")
    log "Updated"
  end

  private

  def log(text)
    puts text
  end

  def check_extracted_snapshot(name)
    puts "Checking extracted snapshot"
    full_name = extracted_snapshot_full_name(name)
    unless Pathname(full_name).exist?
      raise "Cannot found extracted snapshot at #{full_name}"
    end
    full_name
  end

  def extracted_snapshot_full_name(name)
    "#{OUTPUT_DIR}/#{name}"
  end

  def extract_snapshot(name)
    puts "Extracting #{name} from #{snapshot_full_path_tmp}..."
    system("unzip -o '#{snapshot_full_path_tmp}' -d #{OUTPUT_DIR}")
    puts "Done"
  end

  def extract_csv_from_zip(name)
    name = archived_filename
    extract_snapshot(name)
    check_extracted_snapshot(name)
  end

  def archived_filename
    name = `unzip -Z #{snapshot_full_path_tmp} | grep -o -e '#{ARCHIVED_FILENAME_PATTERN}'`&.strip
    if name.empty?
      raise 'No file named #{ARCHIVED_FILENAME_PATTERN} is found it #{}'
    end
    name
  end

  def md5_old
    `md5sum #{snapshot_full_path}`&.split(' ').first
  end

  def md5_new
    `md5sum #{snapshot_full_path_tmp}`&.split(' ').first
  end

  def check_snapshots_md5
    puts "Calculating previous snapshot md5 checksum..."
    old = md5_old
    puts "Calculating new snapshot md5 checksum..."
    new = md5_new
    puts "Old snapshot checksum: #{old}"
    puts "New snapshot checksum: #{new}"
    old == new
  end

  def check_md5_util
    log 'Checking md5sum utility...'
    res = `echo '123' | md5sum`
    unless res&.split(' ').first == 'ba1f2511fc30423bdbb183fe33f3dd0f'
      raise 'md5sum utility is not found or not works as supposed'
    end
  end

  def check_zip_util
    log 'Checking unzip utility...'
    unless system('unzip &>/dev/null')
      raise 'unzip utility is not found or not works as supposed'
    end
  end

  def get_file_name
    n = Nokogiri::HTML(open("#{BASE_URL}/en_output.html"))
    a = n.xpath('//*[@id="mainContent"]/div[2]/ul[1]/li/a')
    raise 'Cannot get a snapshot filename from the website' if a.empty?
    filename = a&.first&.attributes.fetch('href', nil)&.value
    raise 'Snapshot filename is invalid' if filename.empty?
    raise 'Snapshot is in invalid format, expected: .zip' unless filename =~ /.zip$/
    filename
  end

  def check_output_dir
    log "Check if directory exists: #{OUTPUT_DIR}"
    unless File.directory?(OUTPUT_DIR)
      raise "You should create output directory first: #{OUTPUT_DIR}"
    end
  end

  def snapshot_full_path
    "#{OUTPUT_DIR}/#{OUTPUT_FILENAME_ZIP}"
  end

  def snapshot_full_path_tmp
    "#{OUTPUT_DIR}/#{OUTPUT_FILENAME_TMP}"
  end

  def download_snapshot_with_progress(name)
    progress = 0
    old_val = 0
    download_snapshot(name) do |l|
      progress += l
      reported_mb_ago = (progress - old_val) / MB
      next if reported_mb_ago  < 1
      old_val = progress
      log "Downloaded #{progress / MB} MB so far..."
    end
  end

  def download_snapshot(name)
    log "Now downloading a file #{name} to #{snapshot_full_path_tmp}"

    uri = URI("#{BASE_URL}/#{name}")

    Net::HTTP.start(uri.host, uri.port) do |http|
      request = Net::HTTP::Get.new uri

      http.request request do |response|
        open snapshot_full_path_tmp, 'wb' do |io|
          response.read_body do |chunk|
            io.write chunk
            yield(chunk.length)
          end
        end
      end
      log "Downloaded!"
    end
  end
end