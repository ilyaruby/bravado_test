namespace :snapshot do

  desc "Updates companies from an internet snapshot"
  task :update => :environment do
    CompaniesUpdateFromSnapshotService.new.work
  end
end

