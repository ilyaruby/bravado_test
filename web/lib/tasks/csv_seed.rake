require 'csv'

namespace :csv do

  desc "Import CSV Data from Companies dataset"
  task :companies_seed => :environment do
    raise 'There are already some companies in DB, refusing to do an initial seed' unless Company.count == 0

    csv_file_path = 'db/BasicCompanyDataAsOneFile.csv'
    i = 0

    CSV.foreach(csv_file_path, {:headers=>:first_row}) do |row|
      puts "[#{Time.now}] Updating record #{i}" if i % 1000 == 0
      i += 1
      Company.create!({
        :company_name => row[0],
        :company_number => row[1],
        :reg_address_care_of => row[2],
        :reg_address_po_box => row[3],
        :reg_address_address_line1 => row[4],
        :reg_address_address_line2 => row[5],
        :reg_address_post_town => row[6],
        :reg_address_county => row[7],
        :reg_address_country => row[8],
        :reg_address_post_code => row[9],
        :company_category => row[10],
        :company_status => row[11],
        :country_of_origin => row[12],
        :dissolution_date => row[13],
        :incorporation_date => row[14],
        :accounts_account_ref_day => row[15],
        :accounts_account_ref_month => row[16],
        :accounts_next_due_date => row[17],
        :accounts_last_made_up_date => row[18],
        :accounts_account_category => row[19],
        :returns_next_due_date => row[20],
        :returns_last_made_up_date => row[21],
        :mortgages_num_mort_charge => row[22],
        :mortgages_num_mort_outstanding => row[23],
        :mortgages_num_mort_part_satisfied => row[24],
        :mortgages_num_mort_satisfied => row[25],
        :sic_code_sic_text_1 => row[26],
        :sic_code_sic_text_2 => row[27],
        :sic_code_sic_text_3 => row[28],
        :sic_code_sic_text_4 => row[29],
        :limited_partnerships_num_gen_partner => row[30],
        :limited_partnerships_num_lim_partner => row[31],
        :uri => row[32],
        :previous_name_1_condate => row[33],
        :previous_name_1_company_name => row[34],
        :previous_name_2_condate => row[35],
        :previous_name_2_company_name => row[36],
        :previous_name_3_condate => row[37],
        :previous_name_3_company_name => row[38],
        :previous_name_4_condate => row[39],
        :previous_name_4_company_name => row[40],
        :previous_name_5_condate => row[41],
        :previous_name_5_company_name => row[42],
        :previous_name_6_condate => row[43],
        :previous_name_6_company_name => row[44],
        :previous_name_7_condate => row[45],
        :previous_name_7_company_name => row[46],
        :previous_name_8_condate => row[47],
        :previous_name_8_company_name => row[48],
        :previous_name_9_condate => row[49],
        :previous_name_9_company_name => row[50],
        :previous_name_10_condate => row[51],
        :previous_name_10_company_name => row[52],
        :conf_stmt_next_due_date => row[53],
        :conf_stmt_last_made_up_date => row[54]
      })
    end
  end
end
